<?php include("header.php"); ?>
          <div class="inner cover">
          <?php if ($errors_obj->failure) { ?>
          	<div class="errors alert alert-warning" role="alert">
          		<p><strong>Your gallery has not been created, because:</strong><br/><em> <?php Display::display_err_msg($errors_obj); ?></em></p>
          	</div>
           <?php } ?>
            <h1 class="cover-heading">Make a gallery</h1>
            <p class="lead">
            <p>Upload your photos and automatically turn them into a responsive lightweight gallery that you can share with your family or friends.</p>
            <p>You can upload up to <?php echo MAX_FILES;  ?> files, <?php echo MAX_FILESIZE;  ?> MB each.</p>
            <p>&nbsp;</p>
            </p>
            <form onsubmit="uploadSpinner()" action="/" id="file" method="post" enctype="multipart/form-data">
            <div class="form-group">
            <p class="lead">
            <input id="flavor" type="checkbox" name="flavor-checkbox" data-on-color="info" data-off-color="info" data-on-text="Gallery" data-off-text="Slideshow" checked>
            </p>
            <br/>
            <p class="lead">
            <label for="title">Title</label>
            <input id="title" type="text" size="20" name="title" class="form-control" required>
            </p>
            <br/>
          <p class="lead">
          <label for="files">Select images</label>
	          <input style="margin: 0 0 auto; display:block; width: 100%;" class="btn btn-lg btn-success"  type="file" name="file[]" id="files" multiple required/>
	          <br/><br/>
	          <input id="button" type="submit" value="Create gallery" role="button"  type="button" class="btn btn-lg btn-default required">
	          <div id="spinner" style="display:none; margin-top:-20px;"><small>Uploading files. Please wait.</small><br/><img src="/templates/bootstrap/images/ajaxloader.gif"></div>
	  </div>
          </form>
          </p>
       
          </div>
<?php include("footer.php");