<?php include("header.php"); ?>
          <div class="inner cover">
            <h1 class="cover-heading">About</h1>
            <p class="lead">
            <p>Gallerator lets you easily turn your pictures into a lightweight responsive gallery. After uploading the files you will be presented with a unique link to your gallery that you can then share with your friends by email.</p>
            <h3>Known <span style="text-decoration: line-through;">bugs</span> features</h3>
            <p>Gallerator doesn't support BMP files. This is done on purpose since bitmap files are large, randomly suported by web browsers and generally blah. Transparency in PNG files is not supported. Gallerator should be used to share photos and photos as such do not have an alpha channel in them. Finally, multiple file upload is handled by HTML5, so some browsers (e.g. Android native browser) do not support it <strong>yet</strong>. If you are unable to upload multiple files, please use a different browser.</p><p>If you'd like to suggest a feature or contact me for any other reason, please go to <a style="text-decoration:underline;" href="http://www.iamtomek.com/contact-me/">www.iamtomek.com/contact-me/</a>.</p>
            <p>&nbsp;</p>
            </p>
          </div>
<?php include("footer.php");