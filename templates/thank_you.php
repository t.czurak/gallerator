<?php include("header.php"); ?>      
          <div class="inner cover">
            <h1 class="cover-heading">Well done!</h1>
            <p class="lead">
            <p>Your gallery has been created. Below you will find the link.</p>
            <p>Now you might want to share this link via e-mail or text message.</p>
            <blockquote>
  <p><a href="http://gallerator.org?g=<?php echo $slideshow_obj->gallery_id; ?>">http://www.gallerator.org?g=<?php echo $slideshow_obj->gallery_id; ?></a></p>
</blockquote>
            <p>&nbsp;</p>
            </p>
            <?php if ($errors_obj->error_ids) { ?>
            	<div class="inner cover">
          	<div class="errors alert alert-info" role="alert">
          		<p><strong>Please note:</strong>
          		</strong><br/><em> <?php Display::display_err_msg($errors_obj); ?></em></p>
          	</div>
          	</div>
           <?php } ?>
          </div>
<?php include("footer.php"); ?>