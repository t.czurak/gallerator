<?php include("header.php"); ?>
          <div class="inner cover">
            <h1 class="cover-heading">Privacy</h1>
            <p class="lead">
            <p>Galleries created with Gallerator are <em>unlisted</em> meaning that they are not indexed by search engines and only peple having the exact, unique hyperlink can see them.</p>
            <p>There is no authentication, though, so it is not possible to delete or restrict access to the gallery once the link has been shared.</p>  
            <p>&nbsp;</p>
            </p>
          </div>
<?php include("footer.php");