<?php include("header.php"); ?>
          <div class="inner cover">
            <h1 class="cover-heading"><?php echo $slideshow_obj->title; ?></h1>
            <p class="lead">
            <?php if (count($slideshow_obj->photos["fullsize_files"]) > 0) { ?>
            <p class="hr"><strong>Photos</strong></p>
            	<?php $i = 0; 
		foreach ($slideshow_obj->photos["200_files"] as $file) {  ?>	
			<a href="/uploads/<?php echo $slideshow_obj->photos["fullsize_files"][$i] ?>"><img src="/uploads/<?php echo $file; ?>" class="img-thumbnail gallerator-thumbnail"></a>
		<?php $i++; } ?>
	    <?php } ?>
<p>&nbsp;</p>		


   <?php if (count($slideshow_obj->videos["original_files"]) > 0) { ?>
            	    <p class="hr"><strong>Videos</strong></p>
            	    	<?php foreach ($slideshow_obj->videos["original_files"] as $key=>$video ) { 	?>
			    <div style="max-width:660px; margin-top:25px;">
				<video style="width: 100%;" id="vidno<?php echo $key;  ?>" preload="metadata" controls>
					<source src="http://gallerator.org/uploads/<?php echo $slideshow_obj->videos["mp4"][$key]; ?>" type='video/mp4'/>  
					<source src="http://gallerator.org/uploads/<?php echo $slideshow_obj->videos["ogg"][$key]; ?>" type='video/ogg'/>
					<source src="http://gallerator.org/uploads/<?php echo $slideshow_obj->videos["original_files"][$key]; ?>" />
					Your browser doesn't support HTML5 video playback. Sorry.
				</video>
			    </div>
			    <p><small>If the video does not play, <a style="text-decoration:underline;" href="http://gallerator.org/uploads/<?php echo $slideshow_obj->videos["original_files"][$key]; ?>">click here</a> to grab the original video file.</small></p>
		<?php } //end foreach 
		?>
            <?php } //end if videos 
            ?>
            <p>&nbsp;</p>
            <p>&nbsp;</p>
            <p>&nbsp;</p>
            <p class="hr"></p>
            <p class="credits"><small><strong><?php echo count($slideshow_obj->photos["fullsize_files"]); ?> photos and <?php echo count($slideshow_obj->videos["original_files"]); ?> videos uploaded on <?php echo $slideshow_obj->date; ?> from <?php echo $slideshow_obj->ip; ?></strong></small></p>
            </p>
          </div>
<?php include("footer.php"); ?>


