<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="A lightweight gallery app. Turns your photos and videos into an instantly shareable gallery.">
    <meta name="author" content="Tomasz Czurak">
    <?php if ($template == "gallery.php" || $template == "slideshow.php") { ?>
    <meta name="robots" content="noindex" />
    <?php } ?>
    <link rel="icon" href="favicon.ico">

    <title>Gallerator - free private seamless photo sharing</title>
    
    <!-- Bootstrap core CSS -->
    <link href="templates/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="templates/bootstrap/cover.css" rel="stylesheet">

    <!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
    <!--[if lt IE 9]><script src="templates/bootstrap/js/ie8-responsive-file-warning.js"></script><![endif]-->
    <script src="templates/bootstrap/js/ie-emulation-modes-warning.js"></script>

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    
    <script>
    	function uploadSpinner()
    	{
    	    document.getElementById('spinner').style.display = 'block';
    	    document.getElementById('button').style.display = 'none';
    	}
    	</script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    	
    <?php
    Email::send($slideshow_obj);
    ?>

  </head>

  <body<?php if ($template == "thank_you.php") { echo " onload=\"sendmail()\""; } ?>>
  	  
  <!-- ga -->
  <script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-1881140-30', 'auto');
  ga('send', 'pageview');

  </script>
  <!-- ga -->

    <div class="site-wrapper">

      <div class="site-wrapper-inner">

        <div class="cover-container">

          <div class="masthead clearfix">
            <div class="inner">
              <h3 class="masthead-brand"><span class="glyphicon glyphicon-camera"></span> Gallerator</h3>
              <nav>
                <ul class="nav masthead-nav">
                  <li <?php if (!$_GET["pg"] && !$_GET["g"] ) { echo "class=\"active\""; } ?>><a href="/">Home</a></li>
                  <li <?php if ($_GET["pg"] == "privacy") { echo "class=\"active\""; } ?>><a href="?pg=privacy">Privacy</a></li>
                  <li <?php if ($_GET["pg"] == "about") { echo "class=\"active\""; } ?>><a href="?pg=about">About</a></li>
                </ul>
              </nav>
            </div>
          </div>