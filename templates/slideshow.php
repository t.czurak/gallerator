<?php include("header.php"); ?>
          <div class="inner cover">
          <style>
          #toolbox {
          position:relative;
          float:right;
          vertical-align: middle;
          text-align:right;
          font-size:13px;
          cursor:pointer;
          margin-top:18px;
      	}

      	#toolbox:hover {
      		color:#fff;
      	}

      	#toolbox a:hover {
      		text-decoration: none !important;
      	}


          </style>
            <h1 style="border-bottom: 1px solid #ddd; padding-bottom:4px;" class="cover-heading"><?php echo $slideshow_obj->title; ?>
            	<span id="toolbox">
            		<span id="rotate-button"><span class="glyphicon glyphicon-refresh"></span> Rotate the image&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
            		<a id='zoominlink' href="#">
            		<span class="glyphicon glyphicon-zoom-in"></span> Zoom in
            		</a>
            	</span>
            </h1>

            <?php if (count($slideshow_obj->photos["fullsize_files"]) > 0) { ?>
		<div id="carousel-example-generic" class="carousel slide" data-ride="carousel"  data-interval="false">
		

		<?php /*
		 if (count($slideshow_obj->photos["fullsize_files"]) > 1) { ?>
			<ol class="carousel-indicators">
				<?php $i = 0; 
				foreach ($slideshow_obj->photos["660_files"] as $file) {  ?>
				<li data-target="#carousel-example-generic" data-slide-to="<?php echo $i;?>" class="<?php if ( $i == 0) { echo "active"; } ?>"></li>
				<?php $i++; } ?>
			 </ol>
		<?php } */ ?>

		<div class="carousel-inner" role="listbox">
				<?php $i = 0; 
				foreach ($slideshow_obj->photos["660_files"] as $file) {  ?>
				<div class="item <?php if ( $i == 0) { echo "active"; } ?> responsive-img">
				<a href="/uploads/<?php echo $slideshow_obj->photos["fullsize_files"][$i] ?>">
				<img class="gallerator-slideshow" src="/uploads/<?php echo $file; ?>">
				</a></div>
				<?php $i++; } ?>
			</div>
		<?php if (count($slideshow_obj->photos["fullsize_files"]) > 1) { ?>
			<a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
				<span class="glyphicon glyphicon-chevron-left" aria-hidden="true" style="font-size:35px;"></span>
				<span class="sr-only">Previous</span>
				</a>
				<a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
				<span class="glyphicon glyphicon-chevron-right" aria-hidden="true" style="font-size:35px;"></span>
				<span class="sr-only">Next</span>
				</a>
			<?php } ?>
		    </div>
            <?php } // end if photos
            ?> 
          </div>
<script>
$(document).ready(function(){
	var angle = 90;
	var rotated = false;
	$('#rotate-button').click(function(e){
		resizeCarousel();
		item = $("div.active > a > img.gallerator-slideshow");
		item.rotate({animateTo:angle});
		angle += 90;
	});

	$('.carousel-control').click(function(e){
		resizeCarousel(true);
		item = $("div.active > a > img.gallerator-slideshow");
		item.hide();
		item.rotate(0);
		angle = 90;
		item.show();
	});

	$('#zoominlink').click(function(e){
		e.preventDefault();
		$link = $("div.active > a");
		window.location.href = $link[0].href;
	});
	
	function resizeCarousel(reset) {
		//resize the image
		$img = $(".responsive-img");
		var w = parseInt($img.width());
		var h = parseInt($img.height());
		$margin = parseInt( (w - h)/2 );
		if (rotated == true ) { $margin = 0; }
		if ( reset == true ) { angle = 90; $margin = 0; }
		$img.each(function(index, value){
			$selectedImg = $(this);
			$(this).css('margin', $margin + 'px auto');
		});
		console.log(w +" : "+ h);	
		console.log("rotated" + rotated);


		//resize carousel container
		if (rotated == true ) {
			newHeight = w;
		} else { 
			newHeight = h;
		}
		console.log(w +" : "+ h);

		$crsl = $(".carousel-inner");
		$crsl.css('height', newHeight + 'px !important');
		if (rotated === false) { 
			rotated = true; 
		} else if (rotated === true) {
			rotated = false;
		}
		if (rotated == true) {rotated = false; }

	}
	//console.log("New height" + newHeight);
	//console.log("New margin" + $margin + '00px auto');
});

</script>
<?php include("footer.php"); ?>



