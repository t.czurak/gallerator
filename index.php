﻿<?php
include_once("configuration.php");
include_once("classes/classes.php");
	
	switch ($_GET["pg"]) {
		case "privacy":
			Display::render("privacy.php", null, null);
			break;
		case "about":
			Display::render("about.php");
			break;
		default:
			
			if ($_FILES) {
				$errors_obj = new Validator();
						
				if (!$errors_obj->failure) {
					$slideshow_obj = new Slideshow();
					$slideshow_obj->set_gallery($errors_obj->temp_files);
					Display::render("thank_you.php", $errors_obj, $slideshow_obj);
				} else {
					Display::render("homepage.php", $errors_obj, $slideshow_obj);
				}
			} elseif (!$_GET["g"]) {
				Display::render("homepage.php", $errors_obj, $slideshow_obj);
			}
				
			if ($_GET["g"] && !$_GET["f"]) {
				$slideshow_obj = new Slideshow();
				$slideshow_obj->get_gallery($_GET["g"]);
				if ($slideshow_obj->type == "gallery") { Display::render("gallery.php", $errors_obj, $slideshow_obj); }
				if ($slideshow_obj->type == "slideshow") { Display::render("slideshow.php", $errors_obj, $slideshow_obj); }
			} 			 
	}
?>