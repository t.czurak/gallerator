﻿<?php

class Validator {	
	public $error_msgs = array();
	public $error_ids = array();
	public $failure;
	public $temp_files = array();
	private $filetypes_dict = array();
	
	function __construct() {
		
		$this->error_msgs = array(
		1 => "Some image files were too big and have not been uploaded. Allowed filesize is " . MAX_FILESIZE ."MB per file.",
		2 => "Some files were not valid image of video files. Allowed filetypes are: ". ALLOWED_IMG_FILETYPES ." and " . ALLOWED_VID_FILETYPES,
		3 => "No files selected. Select valid image files and try again.",
		4 => "What's your gallery title? Please try again.",
		5 => "You have uploaded more than ". MAX_FILES ." files. Only the first ". MAX_FILES ." valid image files have been included.",
		6 => "The files you uploaded have been all rejected due to filesize/filetype limits."
		);
		
		$this->filetypes_dict = array (
			"1" => "gif",
			"2" => "jpg",
			"3" => "png",
			"6" => "bmp",
		);
		
		#no files uploaded
		if ($_FILES["file"]["error"][0] == 4) {
			array_push($this->error_ids, 3);
			$this->failure = 1;
		} else {
			
			$validated_files_by_size = $this->validate_filesize();
			$validated_files_by_type = $this->validate_filetype();
			
			if (empty($validated_files_by_size)) { $this->failure = 1; }
			if (empty($validated_files_by_type)) { $this->failure = 1; }
			
			#keep only these files that positively validated both tests
			$validated_files = array();
			foreach ($validated_files_by_size as $file) {
				if (in_array($file, $validated_files_by_type)) {
					array_push($validated_files, $file);
					$i++;
				}
				
				#and stop if there are too many
				if ($i == MAX_FILES) { break; }
			}
			
			array_push($this->temp_files, $validated_files);
			
			#if no file passed both validations
			if (empty($this->temp_files)) {
				array_push($this->error_ids, 6);
				$this->failure = 1;
			}
			
			#add file ext to temp_files array
			$fileext = array();		
			
			foreach ($this->temp_files[0] as $key=>$temp_file) {
				$filetype_id = exif_imagetype($temp_file);
				
				if ($filetype_id) { #image			
					$filetype_ext = $this->filetypes_dict[$filetype_id];
					$fileext[$key] = $filetype_ext;
				} else { #something else xxx
					$global_key = array_search($temp_file,  $_FILES["file"]["tmp_name"]);
					$fileext[$key] = substr($_FILES["file"]["name"][$global_key], -3);
					
				}
			}
			array_push($this->temp_files, $fileext);
			
		} //end if $_FILES
		
		$title = trim($_POST["title"]);
		if (empty($title)) {
			array_push($this->error_ids, 4);
			$this->failure = 1;
		}
		
		if (count($_FILES["file"]["name"]) > MAX_FILES) {
			array_push($this->error_ids, 5);	
		}
		
		$this->error_ids = array_unique($this->error_ids);
		
	} //end constructor
	
	protected function validate_filetype() {
		$allowed_img_filetypes = explode(", ", ALLOWED_IMG_FILETYPES);
		$allowed_vid_filetypes = explode(", ", ALLOWED_VID_FILETYPES);
		$validated_files_by_type = array();
		
		foreach ($_FILES["file"]["tmp_name"] as $key=>$file) {
			$filetype_id = exif_imagetype($file);
						
			if ($filetype_id) { #image			
				$filetype_ext = $this->filetypes_dict[$filetype_id];
			} else { #something else
				$filetype_ext = substr($_FILES["file"]["name"][$key], -3);
			}
			
			if (in_array($filetype_ext, $allowed_img_filetypes) || in_array($filetype_ext, $allowed_vid_filetypes)) {
				array_push($validated_files_by_type, $file);
			} else {
				array_push($this->error_ids, 2);
			}	
		}
		return($validated_files_by_type);
	}
	
	protected function validate_filesize() {
		$validated_files_by_size = array();
		$arr_index = 0;
		foreach ($_FILES["file"]["size"] as $filesize) {
			
			#prepare vars for movie
			$filetype_ext = substr($_FILES["file"]["name"][$arr_index], -3);
			$allowed_vid_filetypes = explode(", ", ALLOWED_VID_FILETYPES);
			
			if ($filesize < MAX_FILESIZE * 1024 * 1024 || in_array($filetype_ext, $allowed_vid_filetypes)) {
				array_push($validated_files_by_size, $_FILES["file"]["tmp_name"][$arr_index]);
			} else {
				array_push($this->error_ids, 1);
			}
			$arr_index++;
		}
		return($validated_files_by_size);
	}
}

class Slideshow {
	public $photos = array();
	public $videos = array();
	public $gallery_id;
	public $type;
	public $title;
	public $date;
	public $ip;
	
	public function get_gallery($gallery_id) {
		$db = new PDO(DB_TYPE . ":host=" . DB_HOST . ";dbname=" . DB_NAME . ";charset=" . DB_CHARSET, DB_USER, DB_PASS);
		$query = $db->prepare("SELECT * FROM galleries where gallery_id=?");
		
		#execute the query
		$query->execute(array($gallery_id));
		$row = $query->fetchAll(PDO::FETCH_ASSOC);
		
		#populate the object
		if (!empty($row[0]["fullsize_photos"])) { $this->photos["fullsize_files"] = unserialize($row[0]["fullsize_photos"]); }
		if (!empty($row[0]["medium_photos"])) { $this->photos["200_files"] = unserialize($row[0]["medium_photos"]); }
		if (!empty($row[0]["large_photos"])) { $this->photos["660_files"] = unserialize($row[0]["large_photos"]); }
		
		if (!empty($row[0]["original_videos"])) { $this->videos["original_files"] = unserialize($row[0]["original_videos"]); }
		if (!empty($row[0]["mp4_videos"])) { $this->videos["mp4"] = unserialize($row[0]["mp4_videos"]); }
		if (!empty($row[0]["webm_videos"])) { $this->videos["webm"] = unserialize($row[0]["webm_videos"]); }
		if (!empty($row[0]["ogg_videos"])) { $this->videos["ogg"] = unserialize($row[0]["ogg_videos"]); }
		
		$this->gallery_id = $row[0]["gallery_id"];
		$this->date = $row[0]["date"];
		$this->ip = $row[0]["ip"];
		$this->title = $row[0]["title"];
		$this->type = $row[0]["type"];
		
	}
	
	public function set_gallery($temp_files) {
		$fileprefix = md5(uniqid());
		$img_tmp_files = $this->save_vid_files($temp_files, $fileprefix);
		$temp_files = $img_tmp_files;
		
		#do not save img files if there aren't any
		if (!empty($temp_files[0])) {
			$this->save_img_files($temp_files, $fileprefix);
		} 
		
		$this->set_other_fields();
		$this->write_db();
	}
	
	protected function save_vid_files($temp_files, $fileprefix) {
		
		$allowed_vid_filetypes = explode(", ", ALLOWED_VID_FILETYPES);
		$tmp_files = array();
		$tmp_exts = array();
		$img_tmp_files = array($tmp_files, $tmp_exts);
		$original_files = array();
		$mp4_files = array();
		$webm_files = array();
		$ogg_files = array();
		
		foreach ($temp_files[0] as $key=>$file) {
			
			if (in_array($temp_files[1][$key], $allowed_vid_filetypes)) { # if video, save files
				$fileext = "." . $temp_files[1][$key];
				
				#save original files
				$file_name = $fileprefix . "_original_" . $key . $fileext;
				move_uploaded_file($file, "uploads/" . $file_name);
				array_push($original_files, $file_name);
				
				#save mp4 files
				$mp4_file_name = $fileprefix . "_" . $key . ".mp4";
				$cmd = "ffmpeg -i uploads/" . $file_name . " -crf 0 uploads/" . $mp4_file_name . " > /dev/null 2>/dev/null &";
				shell_exec($cmd);
				array_push($mp4_files, $mp4_file_name);
				
				/*
				#save webm files
				$webm_file_name = $fileprefix . "_" . $key . ".webm";
				$cmd = "ffmpeg -i uploads/" . $file_name . " uploads/" . $webm_file_name . " > /dev/null 2>/dev/null &";
				shell_exec($cmd);
				array_push($webm_files, $webm_file_name);
				*/
				
				#save ogg files
				$ogg_file_name = $fileprefix . "_" . $key . ".ogv";
				$cmd = "ffmpeg -i uploads/" . $file_name . " uploads/" . $ogg_file_name . " > /dev/null 2>/dev/null &";
				shell_exec($cmd);
				array_push($ogg_files, $ogg_file_name);
				
			} else { #else put image files on the stack
				array_push($img_tmp_files[0], $file);
				array_push($img_tmp_files[1], $temp_files[1][$key]);
			}
		}
		
		$this->videos["original_files"] = $original_files;
		$this->videos["mp4"] = $mp4_files;
		$this->videos["webm"] = $webm_files;
		$this->videos["ogg"] = $ogg_files;
		return $img_tmp_files;
		
	}
	
	
	protected function save_img_files($temp_files, $fileprefix) {
		$original_files = array();
		
		foreach ($temp_files[0] as $key=>$file) {
			$fileext = "." . $temp_files[1][$key];
			$file_name = $fileprefix . "_original_" . $key . $fileext;
			move_uploaded_file($file, "uploads/" . $file_name);
			array_push($original_files, $file_name);
			$i++;
		}
		$this->photos["fullsize_files"] = $original_files;
				
		$this->save_resized_files($fileprefix, 200);
		$this->save_resized_files($fileprefix, 660); 
	}
	
	protected function save_resized_files($fileprefix, $newwidth) {
		$filenames = array();
		
		foreach ($this->photos["fullsize_files"] as $key=>$filename) {
			
			#prepare variables
			$fileext = substr($filename, -4);
			$newfilename = $fileprefix . "_" . $newwidth . "px" . "_" . $key . ".jpg";
			array_push($filenames, $newfilename);
			
			#set original and resized image widths and heights
			list($width, $height) = getimagesize("uploads/" . $filename);
			
			$ratio = $newwidth/$width;
			$newheight = $height * $ratio;
			
			#prepare original and resized images in memory		
			switch ($fileext) {
				case ".png" : $source =  imagecreatefrompng("uploads/" . $filename); break;
				case ".jpg" : $source =  imagecreatefromjpeg("uploads/" . $filename); break;
				case ".gif" : $source =  imagecreatefromgif("uploads/" . $filename); break;
			}
			$resized = imagecreatetruecolor($newwidth, $newheight);
			
			#do the resizing
			imagecopyresampled($resized, $source, 0, 0, 0, 0, $newwidth, $newheight, $width, $height);
			
			#save the file to disk and clean up
			imagejpeg($resized, "uploads/" . $newfilename, 98);
			imagedestroy($resized);
			imagedestroy($source);
		}
		
		$this->photos["{$newwidth}_files"]  = $filenames; 
		
	} //end save_resized_files()
	
	protected function set_other_fields() {
		$this->title = htmlspecialchars($_POST["title"]);
		$_POST["flavor-checkbox"] ? $this->type = "gallery" : $this->type = "slideshow" ;
		$this->gallery_id = md5(uniqid());
		$this->date = date("c");
		$this->ip = $_SERVER["REMOTE_ADDR"];
	}
	
	protected function write_db() {
		$db = new PDO(DB_TYPE . ":host=" . DB_HOST . ";dbname=" . DB_NAME . ";charset=" . DB_CHARSET, DB_USER, DB_PASS);
		$query = $db->prepare("INSERT INTO galleries (id, date, ip, title, type, fullsize_photos, medium_photos, large_photos, original_videos, mp4_videos, webm_videos, ogg_videos, gallery_id) VALUES (NULL, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");
		
		#prepare data
		$date = $this->date;
		$ip = $this->ip;
		$title = $this->title;
		$type = $this->type; 
		
		if (!empty($this->photos["fullsize_files"])) { $fullsize_photos = serialize($this->photos["fullsize_files"]); }
		if (!empty($this->photos["200_files"])) { $medium_photos = serialize($this->photos["200_files"]); }
		if (!empty($this->photos["660_files"])) { $large_photos = serialize($this->photos["660_files"]); }
		
		if (!empty($this->videos["original_files"])) { $original_videos = serialize($this->videos["original_files"]); }
		if (!empty($this->videos["mp4"])) { $mp4_videos = serialize($this->videos["mp4"]); }
		if (!empty($this->videos["webm"])) { $webm_videos = serialize($this->videos["webm"]); }
		if (!empty($this->videos["ogg"])) { $ogg_videos = serialize($this->videos["ogg"]); }
		
				
		$gallery_id = $this->gallery_id;
		
		#execute the query
		$query->execute(array($date, $ip, $title, $type, $fullsize_photos, $medium_photos, $large_photos, $original_videos, $mp4_videos, $webm_videos, $ogg_videos, $gallery_id));
	}
}

class Display {
	
	static function display_err_msg($errors_obj) {
		#take errors object as an argument and display the message
		echo "<ul>";
		foreach ($errors_obj->error_ids as $id){
			echo "<li>" . $errors_obj->error_msgs[$id] . "</li>";
		}
		echo "</ul>";
	}
	
	static function render($template, $errors_obj = NULL, $slideshow_obj = NULL) {
		include("templates/" . $template);
	}
	
}

class email {
	public $mailto_string;
	
	static function send($slideshow_obj) {
		$mailto_string = "mailto:friend@example.com?subject=" . $slideshow_obj->title . "&body=" . EMAIL_HEADER . "http://www.gallerator.org?g=" . $slideshow_obj->gallery_id . EMAIL_SIG;
		
		echo "
			<script>
			function sendmail()
			{
				location.href = \"{$mailto_string}\";
			}
			</script>
		";
        }
}
?>